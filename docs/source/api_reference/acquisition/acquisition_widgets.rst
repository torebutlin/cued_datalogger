================
Acquisition Widgets
================

.. autoclass:: datalogger.acquisition.RecordingUIs.BaseWidget
  :members:

.. autoclass:: datalogger.acquisition.RecordingUIs.ChanToggleUI
  :members:

.. autoclass:: datalogger.acquisition.RecordingUIs.ChanConfigUI
  :members:

.. autoclass:: datalogger.acquisition.RecordingUIs.DevConfigUI
  :members:

.. autoclass:: datalogger.acquisition.RecordingUIs.StatusUI
  :members:

.. autoclass:: datalogger.acquisition.RecordingUIs.RecUI
  :members: