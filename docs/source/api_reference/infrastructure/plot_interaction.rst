================
Plot interaction
================

A description.

.. autoclass:: datalogger.api.pyqtgraph_extensions.InteractivePlotWidget
  :members:

.. autoclass:: datalogger.api.pyqtgraph_extensions.CustomPlotWidget
  :members:

.. autoclass:: datalogger.api.pyqtgraph_extensions.CustomViewBox
  :members:

.. autoclass:: datalogger.api.pyqtgraph_extensions.CustomViewMenu
  :members:

.. autoclass:: datalogger.api.pyqtgraph_extensions.CustomUITemplate
  :members:

