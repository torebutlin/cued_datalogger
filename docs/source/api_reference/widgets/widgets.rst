=======
Widgets
=======

.. toctree::
  :maxdepth: 2
  :caption: Contents:

  addonmanager

  channelmetadatawidget

  channelselectwidget

  colormapplotwidget

  dataimportwidget

  frequency_domain_widgets

  mastertoolbox

  modal_fitting_widgets

  plot_widgets

  sonogram_widgets

  time_domain_widgets

  toolbox

