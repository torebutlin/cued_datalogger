=================================
Convenience functions and classes
=================================

A few simple functions and classes are defined in the DataLogger package to 
streamline the implementation of some functionality.

.. autofunction:: datalogger.api.numpy_extensions.to_dB

.. autofunction:: datalogger.api.numpy_extensions.from_dB

.. autoclass:: datalogger.api.numpy_extensions.MatlabList

.. autofunction:: datalogger.api.numpy_extensions.sdof_modal_peak
