================
Acquisition Live Graphs
================

.. autoclass:: datalogger.acquisition.RecordingGraph.LiveGraph
  :members:

.. autoclass:: datalogger.acquisition.RecordingUIs.TimeLiveGraph
  :members:

.. autoclass:: datalogger.acquisition.RecordingUIs.FreqLiveGraph
  :members:

.. autoclass:: datalogger.acquisition.RecordingUIs.LevelsLiveGraph
  :members: