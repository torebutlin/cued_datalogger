========
Analysis
========

A short description of the analysis window.

.. toctree::
  :maxdepth: 2
  :caption: Contents:

  window_structure

  time_domain
  
  frequency_domain
  
  sonogram

  modal_fitting
