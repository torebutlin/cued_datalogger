================
Frequency domain
================

.. autoclass:: datalogger.analysis.frequency_domain.FrequencyDomainWidget
  :members:

.. autoclass:: datalogger.analysis.frequency_domain.FrequencyToolbox
  :members:
