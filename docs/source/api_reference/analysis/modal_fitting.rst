=============
Modal fitting
=============

--------------
Circle fitting
--------------

.. autoclass:: datalogger.analysis.circle_fit.CircleFitWidget
  :members:

.. autoclass:: datalogger.analysis.circle_fit.CircleFitToolbox
  :members:

.. autofunction:: datalogger.analysis.circle_fit.fit_circle_to_data

