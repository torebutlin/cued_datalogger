===========
National Instrument Recorder
===========

.. autoclass:: datalogger.acquisition.NIRecorder.Recorder
  :members:
