===========
Time domain
===========

.. autoclass:: datalogger.analysis.time_domain.TimeDomainWidget
  :members:

.. autoclass:: datalogger.analysis.time_domain.TimeToolbox
