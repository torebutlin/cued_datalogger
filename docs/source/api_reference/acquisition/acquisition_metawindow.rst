===========
National Instrument Recorder
===========

.. autoclass:: datalogger.acquisition.ChanMetaWin.ChanMetaWin
  :members:

.. autoclass:: datalogger.acquisition.ChanMetaWin.CommentBox
  :members: